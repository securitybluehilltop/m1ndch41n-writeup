## ran$om message Write-up

### Category: Cryptography

### Challenge: The forensics team have scraps of a conversation. The key and the last few messages. Could you decrypt them and get the flag?

### File(s): symetric.key and messages

## Solution

#### 1) Key analysis

The name of the key file suggests that the encryption algorithm is symmetric. Also, it contains 32 bytes in hex ASCII value or 64 characters. That is a 256-bit key. Another clue is messaging. Concluding that the requirements for decryption are:

- 256-bit key
- Symmetric
- GCM or CCM

The options are AES or ChaCha20.

#### 2) Message analysis

The file with the messages has 3 components: text, cipher and auth. The text is an encrypted message. The auth is another clue that leads to the Message Authentication Code. The cipher must be the initialization vector or nonce. Judging by the size of the nonce, and since ChaCha20 uses 12 hex bytes or 24 characters, the algorithm must be AES. Because the nonce is a string with 16 hex bytes or 32 characters.

#### 3) Decryption

A python script that decrypts the message was written. The script uses PyCryptodome as the decryption library. The decrypt function requires a dictionary (with the encrypted text, the nonce and the authentication tag) and the secret key. The challenge has all those elements. The requirements for the AES.MODE_GCM class are to provide the binaries for these elements. Therefore all should be "unhexlified".

```python
from Crypto.Cipher import AES
import binascii, os

def decrypt_AES_GCM(encryptedMsg, secretKey):
    (ciphertext, nonce, authTag) = encryptedMsg
    aesCipher = AES.new(secretKey, AES.MODE_GCM, nonce)
    plaintext = aesCipher.decrypt_and_verify(ciphertext, authTag)
    return plaintext

secretKey = b'6d6a8e47124b587e64667682e8a6d4525d9bc04a79360ca956fd315ddc32f7c6'
print("Symetric Key:", secretKey)
print('---***---')

secretKey = binascii.unhexlify(secretKey)

encryptMsgs = [
  {'text': b'5a22f6c8f909b73fc9e8fd', 'cipher': b'4e6726074915dfc579c694a4c5ee90f3', 'auth': b'cf4c640b910cd855a8ed65b908325f52'},
  {'text': b'a30a57526b7b', 'cipher': b'97082df164fa80c026f6f316deba5926', 'auth': b'bc92e4b3b358ce2408706cdd2a97231f'},
  {'text': b'3fc766d1f233adb79a83338fc892', 'cipher': b'ce8013a6d265a5ec692d8e682a4f79be', 'auth': b'ef256c8e0a0ecc09b15a62a8add3c1f6'},
  {'text': b'44df45db2ee167fb194e06d1948b7f7ef1bb0a', 'cipher': b'3d70a11f090ad6cc8f1be72cce5d1554', 'auth': b'dec3ccfe586c280a6e72faf86ff545bd'},
  {'text': b'289fb804f4b0cc1e044b056f8e3d903e22ffface264f7d07', 'cipher': b'a2296a5bca84e282735658f85a9682ed', 'auth': b'f6f4c6ea7466c8c25936d297923ed8ec'},
  {'text': b'698a5d2ecb8f2aec65599d4b32e8fbe7fa379a835e83a2f47eea2b08', 'cipher': b'263c2499946313150e248d9911c2d5e6', 'auth': b'24cb3dae0b802182ce5bb8ac0c6edb6e'},
  {'text': b'12588728dbeb6541c3124b2894ca3f7df7542ecddda0dd459bde8004ae', 'cipher': b'b1a7694de298195418b688fd473028dc', 'auth': b'1e9c881ebf9eb1a84d6bb89ac7741eb6'}
]

for index in range(len(encryptMsgs)):
    encryptedMsg = (binascii.unhexlify(list(encryptMsgs)[index]['text']),
                    binascii.unhexlify(list(encryptMsgs)[index]['cipher']),
                    binascii.unhexlify(list(encryptMsgs)[index]['auth'])
    )
    decryptedMsg = decrypt_AES_GCM(encryptedMsg, secretKey)
    print("decryptedMsg", decryptedMsg)
```

The output is the messages in plain text which leads to a Pastebin web address.

```
decryptedMsg b'hello there'
decryptedMsg b'whazup'
decryptedMsg b'got the money?'
decryptedMsg b'yes, i will wire it'
decryptedMsg b'he could suffer, got it?'
decryptedMsg b'yes, I need a proof of life!'
decryptedMsg b'https://pastebin.com/NsEzvtYW'
```

#### 4) Pastebin

The Pastebin web address got a long string of characters. On close examination, it is a hex string.

A ```bash``` script using ```xxd``` recovers the  binary file. When using the ```file``` command, it reveals to be JPEG image.

```bash
xxd -r -p pastebin.hex > decode
```

Using an image visualization software the following image (*Fig. 1*) contains the flag.

![](./decode)
> Fig 1: *Flag*