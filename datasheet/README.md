## Datasheet Challenge Write-up

### Category: Forensics

### Challenge: We got a password protected file with the specs of our new microphone adapter, but we suspect that there is something else. Try to dig more info about this oune. 

### File(s): aurex.zip

#### 1) File type

Using the command ```file``` on the archive we got the following:

```bash
file aurex.zip 
aurex.zip: Zip archive data, at least v2.0 to extract
```

When trying to unzip we got:

```bash
$ unzip aurex.zip 
Archive:  aurex.zip
[aurex.zip] AurexTi_MIC.pdf password: 
   skipping: AurexTi_MIC.pdf         incorrect password
```

We got to crack the passphrase using ```fcrackzip```:

```bash
$ sudo fcrackzip -v -u -D -p dict.txt aurex.zip 
found file 'AurexTi_MIC.pdf', (size cp/uc  85047/ 95071, flags 9, chk 78e3)


PASSWORD FOUND!!!!: pw == qwerty123456
```


When unzipping the file with the passphrase we gota PDF file. The contents of the PDF file are irrelevant. Then we need to focus on the metadata. We use ```exiftool``` command to access the meta data:

```bash
$ exiftool AurexTi_MIC.pdf 
ExifTool Version Number         : 11.16
File Name                       : AurexTi_MIC.pdf
Directory                       : .
File Size                       : 93 kB
File Modification Date/Time     : 2020:01:25 15:07:06-06:00
File Access Date/Time           : 2020:01:25 15:07:06-06:00
File Inode Change Date/Time     : 2020:01:25 17:58:55-06:00
File Permissions                : rw-r--r--
File Type                       : PDF
File Type Extension             : pdf
MIME Type                       : application/pdf
PDF Version                     : 1.5
Linearized                      : No
Page Count                      : 1
XMP Toolkit                     : Image::ExifTool 11.16
Creator                         : aGlsbHRvcENURnttM3Q0ZDRUNF8xc18xbmYwfQ==
Producer                        : HilltopCTF
Create Date                     : 2020:01:25 15:03:21-06:00
```

We can see that the ```Creator``` tag got a Base64 string. When decoding we get:

```bash
echo "aGlsbHRvcENURnttM3Q0ZDRUNF8xc18xbmYwfQ==" | base64 --decode
HilltopCTF{m3t4d4T4_1s_1nf0}
```
