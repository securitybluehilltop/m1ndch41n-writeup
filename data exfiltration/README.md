## Data Exfiltration Writeup

### Category: Steganography

### Challenge: Find theflag in the data exfiltration

### File(s): Brochure\_financial\_services.eml

## Solution

#### 1) Mail check

The file used for this challenge is an eml. To read the contents and extract the attachments with munpack.

```bash
$ munpack -t Financial\ Services\ and\ Investments.eml
```

And got the following files:

```bash
part1 (text/plain)
part2 (text/html)
brochure.md5 (application/octet-stream)
brochure.pptx (application/vnd.openxmlformats-officedocument.presentationml.presentation)
```

Using ```cat``` to read the contents of the ```part1``` file, there is nothing useful. Same with ```part2```.

Comparing the files using the ```md5sum``` and the ```brochure.md5``` yields an inequality. 

```bash
$ md5sum brochure.pptx
bdde6056c0bc9e200052bcc036e1e3d9  brochure.pptx
```

```bash
$ cat brochure.md5
d3b56fbe39a40856a5d94b846b493135
```
There must be a file alteration or a hint (perhaps both).

##### 2) Powerpoint file check

Reviewing the contents of Powerpoint file, reveals nothing. Actually it is a template. There is no visible informations or hints, therefore the flag must be in the contents.

Powerpoint files are containers and can be unzipped. A working directory (pptx-contents) was created to put the content of the file.

```bash
mkdir pptx-contents && unzip brochure.pptx -d pptx-contents/
```

The new directory has 208 files to check.

#### 3) Digging the contents

The standard tools for steganography include ```file```, ```strings```, ```binwalk``` among others. The first file to check is ```[Content_Types].xml``` at the top hierarchy of the directory.

Using ```strings``` (or ```tail```) reveals the following comment at the end of the file:

```
<!--Passphrase: crypto_volatility_high-->
```
It must be a passfrase to decode some file yet unknown. The most easyway to find the file is to remember the unused ```md5``` file.

The file that matches this ```md5 = d3b56fbe39a40856a5d94b846b493135``` is ```./pptx-contents/ppt/media/image9.jpg``` according to the hash:

```bash
$ md5sum image9.jpg 
d3b56fbe39a40856a5d94b846b493135  image9.jpg
$ cat cat brochure.md5 
d3b56fbe39a40856a5d94b846b493135
```

Being an image file, it is most likely to have an archive with an embed file. The command ```steghide``` can extract the file contained inside it and the passphrase will be the one discovered in the XML.

```bash
$ steghide extract -sf image9.jpg 
Enter passphrase: crypto_volatility_high
wrote extracted data to "clients.csv.gpg".
```

The extracted file is a ```gpg``` file. Attempting to decrypt it will need a passphrase. That is the same we already found.

```bash
$ gpg --batch --passphrase "crypto_volatility_high" clients.csv.gpg
gpg: WARNING: no command supplied.  Trying to guess what you mean ...
gpg: AES256 encrypted data
gpg: encrypted with 1 passphrase
```

The extracted file is ```clients.csv```, which contains a databse with names, addresses, Social Security Numbers, Phone Numbers, Credit Card Numbers and types. This is the exfiltrated information. A ```grep``` on the file will reveal the flag:

```bash
$ grep hilltop clients.csv
Isaiahside, VA 70628",070-87-4464,515.571.4872x98100,americanexpress,HilltopCTF{4A11EC495EE134DEFFFB63DAD54DABFF}
```

The flag is: ```HilltopCTF{4A11EC495EE134DEFFFB63DAD54DABFF}```