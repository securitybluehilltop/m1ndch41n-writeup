## Bitcoin Scam Write-up

### Category: Forensics

### Challenge: We just got the Mastermind behind a investment scam. But before any of the accomplices draw everything from the account we think that you might be able to help with it and we proceed to seize the funds. Can you help us locate and get the funds?

### File(s): email01.eml, email02.eml, email03.eml, homepage.zip

#### 1) Emails

When we examine the emails we got a set of 24 ordered words. Considering that an ordered set of words, they might be a pashphrase. From the homepage we got website that encourages to send bitcoins to a wallet. 

Since we know that the scam involved using bitcoins we could tie the words to a wallet. Organizing the 24 words we got the following sequence

```bash
assist congress detail gather silly afford father loyal chimney rely execute vicious force forget squeeze rate shell rubber roast similar mechanic apart demise enhance
```
When the 24 Mnemonic seed is used to generate the Private Key and the Public Key, we face the problem that there can be a lot of wallets generated with the same words. But we know that a BIP39 tool was used to generate the 24 words.

To narrow it down, the only clue related to it is the following QR code in the homepage:

![QR](./images/QRcode.png)

Using an online site to read QR codes or by scanning the code, we came up with the following wallet:

```bash
1AwgyWmSKwFSWeqUaE2spFEBrxY74vfqER
```
Using [BIP39](https://iancoleman.io/bip39/) by Ian Coleman, we can search all the possibilities.

After enumerating 1500 wallets we get:

```bash
m/44'/0'/0'/0/1112	1AwgyWmSKwFSWeqUaE2spFEBrxY74vfqER	030037e1fb0d6906d99aa91a8f40e66d4726380125c5233f899cbecd41cf7ddaef	KxhPmCtjwV4dgkNFYAvAAkkryvR1U2Hu3i8BnZSca8MT1291ZGhW
```

The flag is:

```bash
hilltopCTF{KxhPmCtjwV4dgkNFYAvAAkkryvR1U2Hu3i8BnZSca8MT1291ZGhW}
```