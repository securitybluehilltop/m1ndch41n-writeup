## hilltop_0x01 Write-up

### Category: Reverse Engineering

### Challenge: Can you get the flag?

### File(s): hilltop_0x01

## Solution A

#### 1) File type

Using the command ```file``` over the executable we can confirm that it is an ELF binary.

```bash
$ file hilltop_0x01 
hilltop_0x01: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=9cd0ef7a86f61ba496a7e726f9ff40abc38ce949, not stripped
```
Testing the binary in a 64-bit terminal, the output is:

```bash
$ ./hilltop_0x01

  .::--- HilltopCTF ---::.

Checking you argument...

Wrong! Try again.
```

Then it can be assumed that the binary expects an argument or string.

#### 2) Command: Strings

It is possible to analyse the executable by detecting the strings coded inside it.

```bash
$ strings hilltop_0x01
/lib64/ld-linux-x86-64.so.2
libc.so.6
...
GursyntvH
fUvyygbcH
PGSor3s3H
os93qr32H
nqnqr175H
op320290H
...
rot13
...
```
This output should hint the challenger that there is hardcoded inside the binary and has a the cypher rot13 to decode the flag.

In order to get the key, the command ```strings``` must be used with the flag ```-n 3``` to show the last part of the key that did not show with above example.

```bash
$ strings -n 3 hilltop_0x01
...
GursyntvH
fUvyygbcH
PGSor3s3H
os93qr32H
nqnqr175H
op320290H
0sn
...
```

Eliminating the H as the delimiter character, then the key is:

```
GursyntvfUvyygbcPGSor3s3os93qr32nqnqr175op3202900sn
```

Using the shell with the rot13 cypher the flag is obtained:

```bash
$ echo 'GursyntvfUvyygbcPGSor3s3os93qr32nqnqr175op3202900sn' | tr 'A-Za-z' 'N-ZA-Mn-za-m'
TheflagisHilltopCTFbe3f3bf93de32adade175bc3202900fa
```

This flag used in the binary ```hilltop_0x01``` gives the correct answer:

```bash
$./hilltop_0x01 TheflagisHilltopCTFbe3f3bf93de32adade175bc3202900fa

  .::--- HilltopCTF ---::.

Checking you argument...

Success!
```

Modifying the argument to meet the flag format we get:
```
HilltopCTF{be3f3bf93de32adade175bc3202900fa}
```

## Solution B


#### 1) Analyse

Using [Radare 2](https://rada.re/n/) on the binary file ```hilltop_0x01``` we got the following information:

```bash
$ rabin2 -I hilltop_0x01
arch     x86
baddr    0x0
binsz    14849
bintype  elf
bits     64
canary   false
sanitiz  false
class    ELF64
crypto   false
endian   little
havecode true
intrp    /lib64/ld-linux-x86-64.so.2
laddr    0x0
lang     c
linenum  true
lsyms    true
machine  AMD x86-64 architecture
maxopsz  16
minopsz  1
nx       true
os       linux
pcalign  0
pic      true
relocs   true
relro    partial
rpath    NONE
static   false
stripped false
subsys   linux
va       true
```
It is an 64bit ELF file, dynamically linked and not stripped. Which means that do not have exploit countermeasures. Running the binary, the following output is obtained:

```bash
$./hilltop_0x01

  .::--- HilltopCTF ---::.

Checking you argument...

Wrong! Try again.
```

And with an argument:

```bash
$./hilltop_0x01 123456

  .::--- HilltopCTF ---::.

Checking you argument...

Wrong! Try again.
```

A passcode must be needed to solve this challenge. Starting with radare2:

```bash
$r2 ./hilltop_0x01
[0x00001070]> 
```

The address is: ```0x00001070```. Moving to the entry points:

```bash
[0x00001070]> ie
[Entrypoints]
vaddr=0x00001070 paddr=0x00001070 haddr=0x00000018 hvaddr=0x00000018 type=program

1 entrypoints
```
With one entry point then proceed to analyse the binary:

```bash
[0x00001070]> aaa
[x] Analyze all flags starting with sym. and entry0 (aa)
[x] Analyze function calls (aac)
[x] Analyze len bytes of instructions for references (aar)
[x] Constructing a function name for fcn.* and sym.func.* functions (aan)
[x] Type matching analysis for all functions (aaft)
[x] Use -AA or aaaa to perform additional experimental analysis.
```

Now on to the flags with the imports:

```bash
[0x00001070]> fs imports; f
0x00000000 16 loc.imp._ITM_deregisterTMCloneTable
0x00001030 6 sym.imp.strcpy
0x00001040 6 sym.imp.puts
0x00000000 16 sym.imp.__libc_start_main
0x00001050 6 sym.imp.strcmp
0x00000000 16 loc.imp.__gmon_start
0x00000000 16 loc.imp._ITM_registerTMCloneTable
0x00000000 16 sym.imp.__cxa_finalize
```
Flags with the strings:

```bash
[0x00001070]> fs strings; f
0x00002004 29 str..::____HilltopCTF____::.
0x00002021 26 str.Checking_you_argument...
0x0000203b 10 str.Success
0x00002045 19 str.Wrong__Try_again.
```

Now it is showing a string for ""Success", therefore we can dig deeper into strings:

```bash
[0x00001070]> axt @@ str.*
main 0x13a8 [DATA] lea rdi, qword str..::____HilltopCTF____::.
main 0x13b4 [DATA] lea rdi, qword str.Checking_you_argument...
main 0x13dd [DATA] lea rdi, qword str.Success
main 0x13eb [DATA] lea rdi, qword str.Wrong__Try_again.
```

And we got the strings inside the ```main``` function. Now into the functions:

```bash
[0x00001070]> afl
0x00001000    3 23           sym._init
0x00001030    1 6            sym.imp.strcpy
0x00001040    1 6            sym.imp.puts
0x00001050    1 6            sym.imp.strcmp
0x00001060    1 6            sub.__cxa_finalize_1060
0x00001070    1 43           entry0
0x000010a0    4 41   -> 34   sym.deregister_tm_clones
0x000010d0    4 57   -> 51   sym.register_tm_clones
0x00001110    5 57   -> 50   sym.__do_global_dtors_aux
0x00001150    1 5            entry.init0
0x00001155   19 381          sym.rot13
0x000012d2    1 199          sym.hilltop
0x00001399    5 101          main
0x00001400    3 93   -> 84   sym.__libc_csu_init
0x00001460    1 1            sym.__libc_csu_fini
0x00001464    1 9            sym._fini
```

We got a clue, there is a function with the ```rot13``` function. Then the input argument will be a string shifted be Caesar Cypher. Second clue is a hilltop function.

#### 2) Disassemble

Using the ```pdf`` (Pring Disassemble Function) we obtain the following:

```bash
[0x00001070]> s main
[0x00001399]> pdf
/ (fcn) main 101
|   main (int argc, char **argv, char **envp);
|           ; var char **local_10h @ rbp-0x10
|           ; var signed int local_4h @ rbp-0x4
|           ; arg signed int argc @ rdi
|           ; arg char **argv @ rsi
|           ; DATA XREF from entry0 (0x108d)
|           0x00001399      55             push rbp
|           0x0000139a      4889e5         mov rbp, rsp
|           0x0000139d      4883ec10       sub rsp, 0x10
|           0x000013a1      897dfc         mov dword [local_4h], edi   ; argc
|           0x000013a4      488975f0       mov qword [local_10h], rsi  ; argv
|           0x000013a8      488d3d550c00.  lea rdi, qword str..::____HilltopCTF____::. ; 0x2004 ; "\n  .::--- HilltopCTF ---::.\n" ; const char *s
|           0x000013af      e88cfcffff     call sym.imp.puts           ; int puts(const char *s)
|           0x000013b4      488d3d660c00.  lea rdi, qword str.Checking_you_argument... ; 0x2021 ; "Checking you argument...\n" ; const char *s
|           0x000013bb      e880fcffff     call sym.imp.puts           ; int puts(const char *s)
|           0x000013c0      837dfc01       cmp dword [local_4h], 1
|       ,=< 0x000013c4      7e25           jle 0x13eb
|       |   0x000013c6      488b45f0       mov rax, qword [local_10h]
|       |   0x000013ca      4883c008       add rax, 8
|       |   0x000013ce      488b00         mov rax, qword [rax]
|       |   0x000013d1      4889c7         mov rdi, rax
|       |   0x000013d4      e8f9feffff     call sym.hilltop
|       |   0x000013d9      85c0           test eax, eax
|      ,==< 0x000013db      740e           je 0x13eb
|      ||   0x000013dd      488d3d570c00.  lea rdi, qword str.Success  ; 0x203b ; "Success!\n" ; const char *s
|      ||   0x000013e4      e857fcffff     call sym.imp.puts           ; int puts(const char *s)
|     ,===< 0x000013e9      eb0c           jmp 0x13f7
|     |||   ; CODE XREFS from main (0x13c4, 0x13db)
|     |``-> 0x000013eb      488d3d530c00.  lea rdi, qword str.Wrong__Try_again. ; 0x2045 ; "Wrong! Try again.\n" ; const char *s
|     |     0x000013f2      e849fcffff     call sym.imp.puts           ; int puts(const char *s)
|     |     ; CODE XREF from main (0x13e9)
|     `---> 0x000013f7      b800000000     mov eax, 0
|           0x000013fc      c9             leave
\           0x000013fd      c3             ret

```


This shows a straight forward binary that takes an argument and if it is correct, prints "Success!", if not, prints "Wrong".

Now its time to zoom to the ```sym.hilltop```:

```bash
[0x000012d2]> pdf
            ;-- rip:
/ (fcn) sym.hilltop 199
|   sym.hilltop (char *arg1);
|           ; var char *src @ rbp-0xc8
|           ; var char *s2 @ rbp-0xc0
|           ; var int local_b8h @ rbp-0xb8
|           ; var int local_b0h @ rbp-0xb0
|           ; var int local_a8h @ rbp-0xa8
|           ; var int local_a0h @ rbp-0xa0
|           ; var int local_98h @ rbp-0x98
|           ; var int local_90h @ rbp-0x90
|           ; var char *dest @ rbp-0x80
|           ; arg char *arg1 @ rdi
|           ; CALL XREF from main (0x13d4)
|           0x000012d2      55             push rbp
|           0x000012d3      4889e5         mov rbp, rsp
|           0x000012d6      4881ecd00000.  sub rsp, 0xd0
|           0x000012dd      4889bd38ffff.  mov qword [src], rdi        ; arg1
|           0x000012e4      488b9538ffff.  mov rdx, qword [src]
|           0x000012eb      488d4580       lea rax, qword [dest]
|           0x000012ef      4889d6         mov rsi, rdx                ; const char *src
|           0x000012f2      4889c7         mov rdi, rax                ; char *dest
|           0x000012f5      e836fdffff     call sym.imp.strcpy         ; char *strcpy(char *dest, const char *src)
|           0x000012fa      48b847757273.  movabs rax, 0x76746e7973727547 ; 'Gursyntv'
|           0x00001304      48ba66557679.  movabs rdx, 0x6362677979765566 ; 'fUvyygbc'
|           0x0000130e      48898540ffff.  mov qword [s2], rax
|           0x00001315      48899548ffff.  mov qword [local_b8h], rdx
|           0x0000131c      48b85047536f.  movabs rax, 0x337333726f534750 ; 'PGSor3s3'
|           0x00001326      48ba6f733933.  movabs rdx, 0x323372713339736f ; 'os93qr32'
|           0x00001330      48898550ffff.  mov qword [local_b0h], rax
|           0x00001337      48899558ffff.  mov qword [local_a8h], rdx
|           0x0000133e      48b86e716e71.  movabs rax, 0x35373172716e716e ; 'nqnqr175'
|           0x00001348      48ba6f703332.  movabs rdx, 0x303932303233706f ; 'op320290'
|           0x00001352      48898560ffff.  mov qword [local_a0h], rax
|           0x00001359      48899568ffff.  mov qword [local_98h], rdx
|           0x00001360      c78570ffffff.  mov dword [local_90h], 0x6e7330 ; '0sn'
|           0x0000136a      488d8540ffff.  lea rax, qword [s2]
|           0x00001371      4889c7         mov rdi, rax
|           0x00001374      e8dcfdffff     call sym.rot13
|           0x00001379      488d9540ffff.  lea rdx, qword [s2]
|           0x00001380      488d4580       lea rax, qword [dest]
|           0x00001384      4889d6         mov rsi, rdx                ; const char *s2
|           0x00001387      4889c7         mov rdi, rax                ; const char *s1
|           0x0000138a      e8c1fcffff     call sym.imp.strcmp         ; int strcmp(const char *s1, const char *s2)
|           0x0000138f      85c0           test eax, eax
|           0x00001391      0f94c0         sete al
|           0x00001394      0fb6c0         movzx eax, al
|           0x00001397      c9             leave
\           0x00001398      c3             ret
```

Got the 8 character lines by address:

```bash
0x000012fa      48b847757273.  movabs rax, 0x76746e7973727547 ; 'Gursyntv'
0x00001304      48ba66557679.  movabs rdx, 0x6362677979765566 ; 'fUvyygbc'
0x0000131c      48b85047536f.  movabs rax, 0x337333726f534750 ; 'PGSor3s3'
0x00001326      48ba6f733933.  movabs rdx, 0x323372713339736f ; 'os93qr32'
0x0000133e      48b86e716e71.  movabs rax, 0x35373172716e716e ; 'nqnqr175'
0x00001348      48ba6f703332.  movabs rdx, 0x303932303233706f ; 'op320290'
0x00001360      c78570ffffff.  mov dword [local_90h], 0x6e7330 ; '0sn'
```
Then ```sym.rot13``` is called:

```bash
0x00001374      e8dcfdffff     call sym.rot13
```

Going back to shell we will use the utility rahash2. Rot13 algorithm is included. We used the flag -E to select a rotation with a shift of 13 characters to the String selected (flags -S s:13 -s). Then we got:

```bash
$ rahash2 -E rot -S s:13 -s 'GursyntvfUvyygbcPGSor3s3os93qr32nqnqr175op3202900sn\n'
TheflagisHilltopCTFbe3f3bf93de32adade175bc3202900fa
```

When we return to radare2 and use ```ood``` to open the binary in debug mode and provide the string we got:

```bash
[0x00001452]> ood
Process with PID 12644 started...
File dbg:///home/canf/Projects/hilltopCTF/reverse_eng/hilltop_0x01/00_source_code/hilltop_0x01  reopened in read-write mode
= attach 12644 12644
WARNING: bin_strings buffer is too big (0xffffaa50c0afe188). Use -zzz or set bin.maxstrbuf (RABIN2_MAXSTRBUF) in r2 (rabin2)
WARNING: bin_strings buffer is too big (0xffffaa50c0afd158). Use -zzz or set bin.maxstrbuf (RABIN2_MAXSTRBUF) in r2 (rabin2)
12644

[0x7fce70cd89d6]> ood TheflagisHilltopCTFbe3f3bf93de32adade175bc3202900fa
Wait event received by different pid 12644
Wait event received by different pid 12657
Process with PID 12709 started...
File dbg:///home/canf/Projects/hilltopCTF/reverse_eng/hilltop_0x01/00_source_code/hilltop_0x01  TheflagisHilltopCTFbe3f3bf93de32adade175bc3202900fa reopened in read-write mode
= attach 12709 12709
Unable to find filedescriptor 3
Unable to find filedescriptor 3
WARNING: bin_strings buffer is too big (0xffffa9f52afd7188). Use -zzz or set bin.maxstrbuf (RABIN2_MAXSTRBUF) in r2 (rabin2)
WARNING: bin_strings buffer is too big (0xffffa9f52afd6158). Use -zzz or set bin.maxstrbuf (RABIN2_MAXSTRBUF) in r2 (rabin2)
12709
```

Just a couple of warnings. Let us see the output:


```bash
[0x7f3bdcc57090]> dc

  .::--- HilltopCTF ---::.

Checking you argument...

Success!
```

The flag is verified.